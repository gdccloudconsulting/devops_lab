#!/bin/bash

# usage: get_jdk.sh <jdk_version> <rpm|tar>
# jdk_version: default 8
# rpm

JDK_VERSION="8"
EXT="rpm"

if [ -n "$1" ]; then
  if [ "$1" == "7" ]; then
    JDK_VERSION="7"
  fi
fi

if [ -n "$2" ]; then
  if [ "$2" == "tar" ]; then
    EXT="tar.gz"
  fi
fi

URL="http://www.oracle.com"
JDK_DOWNLOAD_URL1="${URL}/technetwork/java/javase/downloads/index.html"
JDK_DOWNLOAD_URL2=`curl -s $JDK_DOWNLOAD_URL1 | egrep -o "\/technetwork\/java/\javase\/downloads\/jdk${JDK_VERSION}-downloads-[0-9]+\.html" | head -n 1`
if [ -z "$JDK_DOWNLOAD_URL2" ]; then
#  echo "Could not get jdk download url - $JDK_DOWNLOAD_URL1"
  exit 1
fi

JDK_DOWNLOAD_URL3="${URL}${JDK_DOWNLOAD_URL2}"
JDK_DOWNLOAD_URL4=`curl -s $JDK_DOWNLOAD_URL3 | egrep -o "http\:\/\/download.oracle\.com\/otn-pub\/java\/jdk\/${JDK_VERSION}u[0-9]+\-(.*)+\/jdk-${JDK_VERSION}u[0-9]+(.*)linux-x64.${EXT}" |sort -b -f -r - | head -n 1 `

curl -s -L -O -H "Cookie: oraclelicense=accept-securebackup-cookie" \
  -k $JDK_DOWNLOAD_URL4

echo `basename $JDK_DOWNLOAD_URL4`
